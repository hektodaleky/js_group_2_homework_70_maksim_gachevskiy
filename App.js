import React, { Component } from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import { createStore } from 'redux';
import { Provider, connect } from 'react-redux';

import reducer from './src/store/reducer';
import MainItem from "./src/containers/MainItem";




const store = createStore(reducer);

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>

                   <MainItem/>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        marginTop: 50
    }
});