export const NUMBER = 'NUMBER';
export const EQUALS = 'EQUALS';
export const MINUS = 'MINUS';
export const PLUS = 'PLUS';
export const MULTIPLY = 'MULTIPLY';
export const DIVISION = 'DIVISION';
export const DOT = 'DOT';
export const CLEAR = 'CLEAR';
export const DELETE = 'DELETE';
export const PARENTHESIS = 'PARENTHESIS';
export const addNumber = (val) => {
    return {type: NUMBER, value: val}
};
export const returnResult = () => {
    return {type: EQUALS}
};
export const addMinus = () => {
    return {type: MINUS}
};
export const addPlus = () => {
    return {type: PLUS}
};
export const addMultiply = () => {
    return {type: MULTIPLY}
};
export const addDivision = () => {
    return {type: DIVISION}
};
export const addDot = () => {
    return {type: DOT}
};
export const clear = () => {
    return {type: CLEAR}
};
export const deleteOne = () => {
    return {type: DELETE}
};
export const returnParenthesis = (val) => {
    return {type: PARENTHESIS, value: val}
};