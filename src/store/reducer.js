import * as actionType from "./action";
const initialState = {
        num: ['0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9'],
        val: '',
        isArithmetic: false,
        isMinus: true,
        isOpen: false,
        parenthesisCount: 0
    }
;
const reducer = (state = initialState, action) => {

    switch (action.type) {
        case actionType.NUMBER: {
            let value = state.val;
            value = value + "" + action.value;
            return {...state, val: value, isArithmetic: true, isMinus: true};
        }
        case actionType.MINUS: {
            if (state.isMinus) {
                let value = state.val;
                value = value + "-";
                return {...state, val: value, isMinus: false, isArithmetic: false};
            }
            return state;
        }
        case actionType.PLUS: {
            if (state.isArithmetic) {
                let value = state.val;
                value = value + "+";
                return {...state, val: value, isArithmetic: false, isMinus: false};
            }
            return state;
        }
        case actionType.MULTIPLY: {
            if (state.isArithmetic) {
                let value = state.val;
                value = value + "*";
                return {...state, val: value, isArithmetic: false, isMinus: false};
            }
            return state;
        }
        case actionType.DIVISION: {
            if (state.isArithmetic) {
                let value = state.val;
                value = value + "/";
                return {...state, val: value, isArithmetic: false, isMinus: false};
            }
            return state;
        }
        case actionType.DOT: {
            if (state.isArithmetic) {
                let value = state.val;
                value = value + ".";
                return {...state, val: value, isArithmetic: false, isMinus: false};
            }
            return state;
        }
        case actionType.DELETE: {
            if (state.val.length < 1)
                return state;

            let value = state.val.substring(0, state.val.length - 1);
            let symbol = state.val.substring(state.val.length - 1, state.val.length);
            if (symbol === '-' || symbol === '+' || symbol === '/' || symbol === '*' || symbol === '.')
                return {...state, val: value, isArithmetic: true, isMinus: true};
            else
                return {...state, val: value, isArithmetic: false, isMinus: false};

        }
        case actionType.PARENTHESIS: {


            let sym;
            action.value === '(' ? sym = -1 : sym = 1;
            if (sym === 1&&!state.isOpen)
                return state;
            let value = state.val;
            value = value + "" + action.value;
            const newVal=parseInt(state.parenthesisCount)+parseInt(sym)
            return {
                ...state,
                val: value,
                isArithmetic: true,
                isMinus: true,
                parenthesisCount: newVal,
                isOpen:true
            };

        }
        case actionType.CLEAR: {

            return {...state, val: "", isArithmetic: false, isMinus: true,isOpen: false};
        }
        case actionType.EQUALS: {
            let value
            if (state.parenthesisCount != 0) {
                alert('ОШИБКА С КОЛЛИЧЕСТВОМ СКОБОК!!!');
                return state;
            }
            try{value = eval(state.val) + "";}
            catch(error){
                alert('FATAL ERROR');
                return {...state, val: "", isArithmetic: false, isMinus: true,isOpen: false};

            }

                return {...state, val: value, isArithmetic: true, isMinus: true};





        }

        default:
            return state;
    }


};
export default reducer;