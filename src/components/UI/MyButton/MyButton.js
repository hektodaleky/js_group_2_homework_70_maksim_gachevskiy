import React from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
const MyButton = props => {
    return (

        <TouchableOpacity onPress={props.press}>
            <View style={styles.buttonNumber} backgroundColor={props.color} onPress={props.press}>
                <Text style={styles.textStyle}>{props.title}</Text>
            </View>
        </TouchableOpacity>
    )
};
const styles = StyleSheet.create({
    buttonNumber: {
        width:102.5,
        height:90

    },
    textStyle:{
        textAlign:'center',
        fontSize:20,
        lineHeight:102
    }
});
export default MyButton;
