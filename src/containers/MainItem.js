import React, {Component} from "react";
import {StyleSheet, TextInput, View} from "react-native";
import MyButton from "../components/UI/MyButton/MyButton";
import {connect} from "react-redux";
import {
    addDivision,
    addDot,
    addMinus,
    addMultiply,
    addNumber,
    addPlus,
    clear,
    deleteOne,
    returnParenthesis,
    returnResult
} from "../store/action";


class MainItem extends Component {
    render() {

        return (

            <View style={styles.container}>

                <View style={styles.inp}>
                    <TextInput value={this.props.val} style={styles.text} editable={false} selectTextOnFocus={false}/>
                </View>


                {this.props.number.map((elem, index) => {
                    return <MyButton key={index} press={() => this.props.addNumber(elem)}
                                     title={elem}
                                     color="#ccc"/>
                })}
                <MyButton press={() => this.props.addPlus()}
                          title={'+'}
                          color="#b3e5b6"/>
                <MyButton press={() => this.props.addMinus()}
                          title={'-'}
                          color="#b3e5b6"/>
                <MyButton press={() => this.props.addMultiply()}
                          title={'*'}
                          color="#b3e5b6"/>
                <MyButton press={() => this.props.addDivision()}
                          title={'/'}
                          color="#b3e5b6"/>
                <MyButton press={() => this.props.clear()}
                          title={'CE'}
                          color="#b3e5b6"/>
                <MyButton press={() => this.props.addDot()}
                          title={'.'}
                          color="#b3e5b6"/>
                <MyButton press={() => this.props.returnParenthesis('(')}
                          title={'('}
                          color="#b3e5b6"/>
                <MyButton press={() => this.props.returnParenthesis(')')}
                          title={')'}
                          color="#b3e5b6"/>
                <MyButton press={() => this.props.deleteOne()}
                          title={'<'}
                          color="#b3e5b6"/>
                <MyButton press={() => this.props.returnResult()}
                          title={'='}
                          color="#b3e5b6"/>

            </View>
        );
    }
}
;
const mapStateToProps = state => {
    return {
        number: state.num,
        val: state.val

    }
};

const mapDispatchToProps = dispatch => {
    return {
        addNumber: (num) => dispatch(addNumber(num)),
        returnResult: () => dispatch(returnResult()),
        addPlus: () => dispatch(addPlus()),
        addMinus: () => dispatch(addMinus()),
        addMultiply: () => dispatch(addMultiply()),
        addDivision: () => dispatch(addDivision()),
        addDot: () => dispatch(addDot()),
        clear: () => dispatch(clear()),
        deleteOne: () => dispatch(deleteOne()),
        returnParenthesis: (num) => dispatch(returnParenthesis(num))


    };
};
const styles = StyleSheet.create({
    container: {

        flex: 1,
        alignSelf: 'flex-end',
        flexDirection: 'row',
        width: '100%',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        flexWrap: 'wrap'


    },
    inp: {
        width: '100%',
        height: 230,
        justifyContent: 'flex-end',

    },
    text: {
        backgroundColor: '#eee',
        fontSize: 90
    }

});
export default connect(mapStateToProps, mapDispatchToProps)(MainItem);